from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import PlanOfCare, CareProvider 

class PlanOfCareAdminSite(AdminSite):
    site_header = 'JHWC Plan of Care Admin'
    site_title = 'JHWC Plan of Care Admin Portal'
    index_title = 'Welcome to JHWC Plan of Care Portal'

plan_of_care_admin_site = PlanOfCareAdminSite(name='plan_of_care_admin')

# Register your models here.
@admin.register(PlanOfCare)
class PlanOfCareAdmin(admin.ModelAdmin):
    list_display = ['date', 'problem']
    list_filter = ['date']

@admin.register(CareProvider)
class CareProviderAdmin(admin.ModelAdmin):
    list_display = ['name', 'role']

plan_of_care_admin_site.register(PlanOfCare, admin_class=PlanOfCareAdmin)
plan_of_care_admin_site.register(CareProvider, admin_class=CareProviderAdmin)

