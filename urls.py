from django.urls import path
from . import views

app_name = 'plan_of_care'

urlpatterns = [
    path('plan_of_care/<int:pk>', views.PlanOfCareList.as_view(), name='plan-list'),
    path('plan_of_care/create/', views.PlanOfCareCreate.as_view(), name='plan-create'),
    path('plan_of_care/create/<int:pk>', views.PlanOfCareCreate.as_view(), name='plan-create'),
    path('plan_of_care/<int:pk>/update', views.PlanOfCareUpdate.as_view(), name='plan-update'),
    path('plan_of_care/<int:pk>/delete', views.PlanOfCareDelete.as_view(), name='plan-delete'),
]
