import django_tables2 as tables
from .models import PlanOfCare 
from django_tables2.utils import A

class PlanOfCareTable(tables.Table):
    change = tables.LinkColumn('geriatrics:plan_of_care:plan-update', args=[A('pk')],
            attrs={'a': {'class': 'update-plan btn btn-warning btn-sm'}}, text='Edit')
    remove = tables.LinkColumn('geriatrics:plan_of_care:plan-delete', args=[A('pk')],
            attrs={'a': {'class': 'delete-plan btn btn-danger btn-sm'}}, text='Delete')

    class Meta:
        model = PlanOfCare
        template_name = 'core/bootstrap.html'
        exclude = ('id', 'admission',)


