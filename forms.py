from django import forms
from .models import PlanOfCare
from bootstrap_modal_forms.forms import BSModalModelForm

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class PlanOfCareCreateForm(BSModalModelForm):
    class Meta:
        model = PlanOfCare
        fields = '__all__' 
        widgets = { 'date': DateInput(), }
