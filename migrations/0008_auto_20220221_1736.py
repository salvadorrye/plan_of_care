# Generated by Django 3.1.6 on 2022-02-21 09:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geriatrics', '0024_auto_20220213_0902'),
        ('plan_of_care', '0007_auto_20220213_1344'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='careprovider',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='planofcare',
            options={'ordering': ['-date'], 'verbose_name_plural': 'Plans of care'},
        ),
        migrations.RenameField(
            model_name='careprovider',
            old_name='abbreviation',
            new_name='role',
        ),
        migrations.RemoveField(
            model_name='planofcare',
            name='problem',
        ),
        migrations.AlterField(
            model_name='planofcare',
            name='admission',
            field=models.ForeignKey(limit_choices_to={'discharged': False}, null=True, on_delete=django.db.models.deletion.CASCADE, to='geriatrics.admission'),
        ),
        migrations.DeleteModel(
            name='Management',
        ),
    ]
