# Generated by Django 3.1.6 on 2022-02-13 05:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plan_of_care', '0006_auto_20220213_1037'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='planofcare',
            options={'ordering': ['-date', 'admission'], 'verbose_name_plural': 'Plans of care'},
        ),
    ]
