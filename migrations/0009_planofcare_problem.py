# Generated by Django 3.1.6 on 2022-02-21 23:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kardex', '0008_auto_20220213_0942'),
        ('plan_of_care', '0008_auto_20220221_1736'),
    ]

    operations = [
        migrations.AddField(
            model_name='planofcare',
            name='problem',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='kardex.diagnosis'),
        ),
    ]
