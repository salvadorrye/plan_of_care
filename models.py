from django.db import models
from geriatrics.models import Admission
from kardex.models import Diagnosis

# Create your models here.
class CareProvider(models.Model):
    name = models.CharField(max_length=70)
    role = models.CharField(max_length=10, blank=True)
    
    class Meta:
        ordering = ['name',]

    def __str__(self):
        return f'{self.name} - {self.role}' 

class PlanOfCare(models.Model):
    date = models.DateField(auto_now=False, null=False, blank=False)
    admission = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False}, null=True)
    problem = models.ForeignKey(Diagnosis, on_delete=models.CASCADE, null=True)
    goal = models.TextField()
    action_plan = models.TextField()
    responsible_person = models.ManyToManyField(CareProvider)

    class Meta:
        ordering = ['-date',]
        verbose_name_plural = 'Plans of care'

    def __str__(self):
        return f'{self.date} - for {self.problem}'

