from django.apps import AppConfig


class PlanOfCareConfig(AppConfig):
    name = 'plan_of_care'
