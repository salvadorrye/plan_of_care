from django.views.generic import ListView
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import PlanOfCare
from .forms import PlanOfCareCreateForm
from geriatrics.models import Admission

from .tables import PlanOfCareTable

from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalDeleteView

class PlanOfCareList(LoginRequiredMixin, ListView):
    model = PlanOfCare
    template_name = 'plan_of_care/includes/plan_of_care.html'

    def get_context_data(self, *args, **kwargs):
        request = self.request
        context = super(PlanOfCareList, self).get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                data = PlanOfCare.objects.filter(admission=admission)
                if data:
                    plans_table = PlanOfCareTable(data)
                    plans_table.paginate(page=request.GET.get("page", 1), per_page=5)
                    context['table'] = plans_table
                else:
                    context['table'] = None
            except Admission.DoesNotExist:
                context['table'] = None
        return context

class PlanOfCareCreate(LoginRequiredMixin, BSModalCreateView):
    model = PlanOfCare
    form_class = PlanOfCareCreateForm
    success_message = 'Plan Of Care was created.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_form(self, *args, **kwargs):
        form = super(PlanOfCareCreate, self).get_form(*args, **kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                form.fields['admission'].initial = admission
                self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
            except Admission.DoesNotExist:
                pass
        return form

    def get_context_data(self, **kwargs):
        context = super(PlanOfCareCreate, self).get_context_data(**kwargs)
        context['title'] = 'Create Plan of Care'
        context['btn'] = 'Create'
        return context

class PlanOfCareUpdate(LoginRequiredMixin, BSModalUpdateView):
    model = PlanOfCare
    form_class = PlanOfCareCreateForm
    template_name = 'plan_of_care/planofcare_form.html'
    success_message = 'Plan Of Care was updated.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        plan = super(PlanOfCareUpdate, self).get_object(*args, **kwargs)
        admission = plan.admission
        #added_by = self.request.user
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return plan 

    def get_context_data(self, **kwargs):
        context = super(PlanOfCareUpdate, self).get_context_data(**kwargs)
        context['title'] = 'Update Plan Of Care'
        context['btn'] = 'Update'
        return context

class PlanOfCareDelete(LoginRequiredMixin, BSModalDeleteView):
    model = PlanOfCare
    success_message = 'Plan Of Care deleted.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        plan = super(PlanOfCareDelete, self).get_object(*args, **kwargs)
        admission = plan.admission
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return plan
